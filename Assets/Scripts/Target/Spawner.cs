using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private Enemy[] enemies;
    [SerializeField] private AudioSource _audios;
    public static Spawner Instance;
    private Queue<Enemy> Duck = new Queue<Enemy>();
    private Queue<Enemy> Circle = new Queue<Enemy>();
    private Queue<Enemy> SecondDuck = new Queue<Enemy>();

    private float timeBetweenWaves = 1f;
    private float nextTimeForWave = 0.5f;

    private void Awake()
    {
        if (Instance == null) { Instance = this; }
    }

    private void Update()
    {
        Init();
    }

    private void Init()
    {
        if (Time.timeSinceLevelLoad >= nextTimeForWave)
        {
            SpawnWave();
            nextTimeForWave = Time.timeSinceLevelLoad + timeBetweenWaves;
        }
    }

    private void SpawnWave()
    {
        int waveCount = Random.Range(0, 4);

        for (int i = 0; i < waveCount; i++)
        {
            int row = Random.Range(0, 3) + 1;
            SpawnTarget(row);
        }
    }

    private Enemy SpawnTarget(int row)
    {
        bool isDuck = GetIsDuck(row);
        Vector3 location = new Vector3((isDuck ? Random.Range(-7f, 7f) : Random.Range(-5f, 5f)), 0, 0);
        Vector3 position = (location.x < 0 ? new Vector3(-10, 0) : new Vector3(10, 0));

        Enemy enemy = GetFromPool(row, position);
        return enemy;
    }

    private Enemy GetFromPool(int row, Vector3 position)
    {
        Queue<Enemy> pool = null;

        switch (row)
        {
            case 0:
                pool = Duck;
                break;
            case 1:
                pool = Circle;
                break;
            case 2:
                pool = SecondDuck;
                break;
        }

        if (pool == null) { pool = Duck; }
        Enemy enemy = pool.Count == 0 ? EnqueueEnemy(row, position, pool) : DequeueEnemy(position, pool);
        return enemy;
    }

    private Enemy EnqueueEnemy(int row, Vector3 position, Queue<Enemy> pool)
    {
        Enemy enemy = Instantiate(GetPrefab(row), position, Quaternion.identity, transform);
        enemy.IsOut += () => { pool.Enqueue(enemy); };
        return enemy;
    }

    private Enemy DequeueEnemy(Vector3 position, Queue<Enemy> pool)
    {
        Enemy enemy = pool.Dequeue();
        enemy.IsOut -= () => { pool.Dequeue(); };
        enemy.transform.position = position;
        enemy.gameObject.SetActive(true);
        return enemy;
    }

    private Enemy GetPrefab(int row)
    {
        switch (row)
        {
            case 0:
                return enemies[0];
            case 1:
                return enemies[1];
            case 2:
                return enemies[2];
        }
        return enemies[0];
    }

    private bool GetIsDuck(int row)
    {
        return row != 2;
    }

    public void HitSound()
    {
        _audios.Play();
    }
}
