using UnityEngine;

public class Crosshair : MonoBehaviour
{
    [SerializeField] private Camera _camera;

    private void Awake()
    {
        _camera = Camera.main;
    }

    private void Update()
    {
        Move();
    }

    private void Move()
    {
        Vector2 mousePosition = _camera.ScreenToWorldPoint(Input.mousePosition);
        Cursor.visible = mousePosition.x < -8f || mousePosition.x > 8f || mousePosition.y < -2f || mousePosition.y > 3f;

        transform.position = new Vector3(Mathf.Clamp(mousePosition.x, -8f, 8f), Mathf.Clamp(mousePosition.y, -2f, 3f));
    }
}
