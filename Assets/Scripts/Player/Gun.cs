using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _spriteRenderer = null;
    [SerializeField] private Camera _camera = null;
    [SerializeField] private float _offset = 1.5f;
    [SerializeField] private AudioSource _audioSource;

    private void Awake()
    {
        _camera = Camera.main;
    }

    private void Update()
    {
        Move();
        Shoot();
    }

    private void Move()
    {
        Vector2 mousePosition = GetMousePosition();
        bool flip = mousePosition.x < 0;
        _spriteRenderer.flipX = flip;
        transform.position = new Vector3(Mathf.Clamp(mousePosition.x + (flip ? -_offset : _offset), -8f, 8f), transform.position.y);
    }

    private Vector2 GetMousePosition()
    {
        return _camera.ScreenToWorldPoint(Input.mousePosition);
    }

    private void Shoot()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 mousePosition = GetMousePosition();
            Collider2D collider = Physics2D.OverlapCircle(mousePosition, 0.01f);
            ShootSound();
            DestroyTarget(mousePosition, collider);
        }
    }

    public void ShootSound()
    {
        _audioSource.Play();
    }

    private static void DestroyTarget(Vector2 mousePosition, Collider2D collider)
    {
        if (collider != null && collider.CompareTag("Target"))
        {
            Enemy enemy = collider.GetComponentInParent<Enemy>();
            enemy.Hit(mousePosition);
        }
    }
}
