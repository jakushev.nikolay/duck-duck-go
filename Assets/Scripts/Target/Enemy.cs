using System;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _spriteRenderer = null;
    [SerializeField] private Animator _targetAnimator;
    [SerializeField] private Transform _transformRotate;
    [SerializeField] private float _stayTime;
    

    public bool IsHit { get; private set; }
    public event Action IsOut;

    private Vector3 _location;
    private float _speed = 2.5f;

    private bool isMoving = false;
    private bool isStaying = false;
    private bool isMovingOut = false;

    private float timeLeft = 0f;

    private void OnEnable()
    {
        IsHit = false;
        isMoving = false;
        isStaying = false;
        isMovingOut = false;
        _targetAnimator.enabled = false;
        _transformRotate.rotation = Quaternion.identity;
    }

    private void Update()
    {
        Create();
    }

    private void Create()
    {
        if (isStaying) { timeLeft -= Time.deltaTime; }
        Move();
    }

    private void Move()
    {
        transform.position = (Mathf.Abs(transform.position.x - _location.x) < 0.01f) ? _location : Vector3.Lerp(transform.position, _location, _speed * Time.deltaTime);
        if (isMovingOut) { gameObject.SetActive(false); IsOut?.Invoke(); return; }
    }

    private void MoveOut()
    {
        isMovingOut = true;
        isMoving = false;
        isStaying = false;
        _location = (_location.x < 0 ? new Vector3(-10, 0) : new Vector3(10, 0));
    }

    public void Hit(Vector2 position)
    {
        if (IsHit) { return; }
        IsHit = true;
        _targetAnimator.enabled = true;
        Spawner.Instance.HitSound();
        MoveOut();
    }    
}
