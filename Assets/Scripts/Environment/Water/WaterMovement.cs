using UnityEngine;

public class WaterMovement : MonoBehaviour
{
    [SerializeField] private float _speed = 0.5f;

    private void Update()
    {
        Move();
    }

    private void Move()
    {
        transform.position = new Vector3(Mathf.Cos(Time.time), Mathf.Sin(Time.time)) * _speed;
    }

}
